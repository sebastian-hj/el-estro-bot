## Introduction

### Description

Telegram bot for miscellaneous uses. Currently:

- Send quote from text file at specified times or when requested.

### Commands

- `\start`: show bot information.
- `\quote`: send quote from text file.

## Installation

### Requirements

- Python 2.7.x and 3.6.x.
- Telegram Python API: [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot).

### Configuration File

A _configuration.json_ file is used to enable different features and specify their settings.

#### Telegram Bot Configuration

    "bot": {
        "token": "",
        "username": "",
        "first_name": "",
        "valid_chat_ids": [
        ]
    }

- `bot.token`: Telegram bot token obtained from the BotFather.
- `bot.username`: Telegram bot username, ends up in _Bot_ or _bot_.
- `bot.first_name`: Telegram bot name.
- `bot.valid_chat_ids`: Valid Telegram chat IDS. For cases where you don't want the whole world to use your bot.

#### Quotes Configuration

    "quotes": {
        "enable": true,
        "file_path": ""
    }

- `quotes.enable`: `true` to enable, `false` to disable.
- `quotes.file_path`: Path for text file of quotes. Expectation is one quote per line.
