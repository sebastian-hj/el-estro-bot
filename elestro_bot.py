# -*- coding: utf-8 -*-

import sys
import argparse
import json
# import logging
from datetime import datetime
from random import sample
import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters


# logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

# Global Variables
python_version = 0
bot_valid_chat_ids = list()
quotes_lines = list()
counter = 0


def set_python_version():
    global python_version

    if sys.version_info.major == 3:
        python_version = 3
    else:
        python_version = 2


def load_json_to_dict(file_path):
    try:
        with open(file_path, 'r') as f_id:
            json_dict = json.load(f_id)
    except IOError:
        print('ERROR: Cannot open file \'{0}\''.format(file_path))
        json_dict = ''
    except ValueError:
        print('ERROR: Is \'{0}\' a JSON file?'.format(file_path))
        json_dict = ''

    return json_dict


def load_file_to_lines(file_path):
    try:
        with open(file_path, 'r') as f_id:
            file_lines = f_id.readlines()
    except IOError:
        print('ERROR: Cannot open file \'{0}\''.format(file_path))
        file_lines = ''
    except ValueError:
        print('ERROR: Cannot decode \'{0}\''.format(file_path))
        file_lines = ''

    return file_lines


def generate_random_list(a_list):
    return sample(a_list, len(a_list))


def validate_bot_config(config_dict):
    if 'bot' not in config_dict:
        sys.exit('ERROR: No \'bot\' field in JSON file!')
    else:
        bot_fields = ['token', 'username', 'first_name', 'valid_chat_ids']
        for field in bot_fields:
            if field not in config_dict['bot']:
                sys.exit('ERROR: No \'{0}\' subfield in \'bot\' field!'.format(field))
            elif not config_dict['bot'][field]:
                sys.exit('ERROR: \'{0}\' subfield is empty!'.format(field))


def get_timestamp():
    return datetime.now().strftime('%Y%m%d%A_%H%M%S')


def validate_chat_id(bot, update):
    # Get message and user
    if python_version == 3:
        message = update.message.text
        user = update.message.chat.first_name
    else:
        message = update.message.text.encode('utf-8')
        user = update.message.chat.first_name.encode('utf-8')

    print('[{0}] Received message from \'{1}\': {2}'.format(get_timestamp(), user, message))

    if update.message.chat_id in bot_valid_chat_ids:
        return True
    else:
        bot.send_message(chat_id=update.message.chat_id, text='Unauthorized user!')
        return False


def send_message(bot, update, text):
    bot.send_message(chat_id=update.message.chat_id, text=text)


def command_start(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Soy El Estro bot!\n'
                                  '/quote: send quote (beta)')


def command_quote(bot, update):
    global counter

    # Show 'typing' action
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    if validate_chat_id(bot, update) is True:
        if not quotes_lines:
            message = 'Sorry. Quotes are not available or not working right now...'
        else:
            # Select random line
            message = quotes_lines[counter]
            counter += 1

            if counter >= len(quotes_lines):
                # Reset counter
                counter = 0
                message += '\n[Reached end of quotes...]'

        send_message(bot, update, message)


def command_echo(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Sorry. Cannot hold a conversation yet...\n¯\_(ツ)_/¯')


def command_unknown(bot, update):
    if validate_chat_id(bot, update) is True:
        send_message(bot, update, 'Invalid command!')


if __name__ == '__main__':
    # Create parser
    parser = argparse.ArgumentParser(description='Run El Estro Bot')
    parser.add_argument('config_json', help='Path to configuration JSON', type=str)
    # Get parser results
    args = parser.parse_args()

    # Set Python version
    set_python_version()

    # Open JSON file to dict
    config_dict = load_json_to_dict(args.config_json)
    if not config_dict:
        sys.exit('ERROR opening configuration JSON')

    # Validate bot configuration
    validate_bot_config(config_dict)

    # Set quotes file path if available
    if 'quotes' in config_dict and 'file_path' in config_dict['quotes']:
        quotes_file_path = config_dict['quotes']['file_path']
        # Read file to lines
        quotes_lines = load_file_to_lines(quotes_file_path)
        # Generate random list
        quotes_lines = generate_random_list(quotes_lines)

    # Get token, username, and first name
    bot_token = config_dict['bot']['token']
    bot_username = config_dict['bot']['username']
    bot_first_name = config_dict['bot']['first_name']
    # Get valid chat IDs
    bot_valid_chat_ids = config_dict['bot']['valid_chat_ids']

    # Create instanse of Telegram Bot
    bot = telegram.Bot(token=bot_token)

    # Check if credentials are correct
    if bot_username != bot.username or bot_first_name != bot.first_name:
        sys.exit('ERROR: Bot name does not match...')

    # Create 'Updater' object
    updater = Updater(token=bot_token)
    # Create 'Dispatcher' object
    dispatcher = updater.dispatcher

    # Use 'CommandHandler' subclass to call 'start' function called every time the Bot receives a Telegram message that
    # contains the '/start' command
    start_handler = CommandHandler('start', command_start)
    dispatcher.add_handler(start_handler)

    # Like above but for '/quote'
    quote_handler = CommandHandler('quote', command_quote)
    dispatcher.add_handler(quote_handler)

    # Reply to any message
    echo_handler = MessageHandler(Filters.text, command_echo)
    dispatcher.add_handler(echo_handler)

    # Unknown handler for unknown commands
    unknown_handler = MessageHandler(Filters.command, command_unknown)
    dispatcher.add_handler(unknown_handler)

    # Start bot
    updater.start_polling()
    updater.idle()
